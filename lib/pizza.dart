class Pizza {
  final String id;
  final String name;
  final String image;
  final double price;
  // final String base;
  // final String category;
  final List<String> ingredients;

  const Pizza({
    required this.name,
    required this.id,
    required this.image,
    required this.price,
    // required this.base,
    // required this.category,
    required this.ingredients,
  });

  factory Pizza.fromJson(Map<String, dynamic> json) => Pizza(
        name: json['name'] as String,
        id: json['id'] as String,
        image: json['image'] as String,
        price: (json['price'] as num).toDouble(),
        // base: json['base'] as String,
        // category: json['category'] as String,
        ingredients: (json['ingredients'] as List).cast<String>(),
      );
}
