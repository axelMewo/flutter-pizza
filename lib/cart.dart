import 'package:pizzas/pizza.dart';

class CartItem {
  final Pizza pizza;
  int quantity;

  CartItem({required this.pizza, this.quantity = 1});
}

class Cart {
  List<CartItem> items = [];

  void addToCart(Pizza pizza, {int quantity = 1}) {
    items.add(pizza.id as CartItem);
  }

  void removeFromCart(Pizza pizza) {
    items.removeWhere((item) => item.pizza.id == pizza.id);
  }

  double getTotal() {
    double total = 0;
    for (var item in items) {
      total += item.pizza.price * item.quantity;
    }
    return total;
  }
}
