import 'package:flutter/material.dart';
import 'package:pizzas/cart.dart';
import 'package:pizzas/pizza.dart';
import 'package:dio/dio.dart';

void main() {
  runApp(const MainApp());
}

const String url = "https://pizzas.shrp.dev";

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const PizzaListScreen(),
      routes: {
        '/pizza-details': (context) => const PizzaDetailsScreen(),
      },
    );
  }
}

class PizzaListScreen extends StatelessWidget {
  const PizzaListScreen({super.key});
  // static const Cart cart;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pizzas"),
      ),
      body: FutureBuilder<List<Pizza>>(
        future: _getPizzasFromAPI(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              final List<Pizza> pizzas = snapshot.data!;

              return ListView.builder(
                itemCount: pizzas.length,
                itemBuilder: (BuildContext context, int index) {
                  final Pizza pizza = pizzas[index];

                  return ListTile(
                    leading: Image.network("$url/assets/${pizza.image}"),
                    title: Text(
                      "${pizza.name} - ${pizza.price} €",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      )
                    ),
                    subtitle: Text(pizza.ingredients.join(', ')),
                    trailing: 
                    ElevatedButton(
                      onPressed: () {
                        // cart.addToCart(pizza);
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Pizza ajoutée au panier')),
                        );
                      },
                      child: const Text('Ajouter au panier'),
                    ),
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        '/pizza-details',
                        arguments: pizza,
                      );
                    },
                  );
                },
              );
            } else if (snapshot.hasError) {
              return const Text("Error");
            }
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Future<List<Pizza>> _getPizzasFromAPI() async {
    try {
      final Response response =
          await Dio().get("$url/items/pizzas");

      final List<Pizza> pizzas = response.data["data"]
          .map<Pizza>((pizzaJson) => Pizza.fromJson(pizzaJson))
          .toList();
      return pizzas;
    } catch (e) {
      print("Error fetching pizzas: $e");
      throw Exception("Can't get pizzas from API");
    }
  }
}

Future<Pizza> _getPizzaDetailsFromAPI(String pizzaId) async {
  try {
    final Response response =
        await Dio().get("$url/items/pizzas/$pizzaId");

    final Map<String, dynamic> pizzaJson = response.data;
    final Pizza pizza = Pizza.fromJson(pizzaJson);
    return pizza;
  } catch (e) {
    print("Error fetching pizza details: $e");
    throw Exception("Can't get pizza details from API");
  }
}




class PizzaDetailsScreen extends StatelessWidget {
  const PizzaDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final Pizza pizza = ModalRoute.of(context)!.settings.arguments as Pizza;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Pizza Details"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.network(
              'https://pizzas.shrp.dev/assets/${pizza.image}',
              width: 200,
            ),
            const SizedBox(height: 20),
            Text(
              pizza.name,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            const Text(
              'Ingredients:',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            ...pizza.ingredients.map((ingredient) => Text(ingredient)),
            const SizedBox(height: 10),
            Text(
              'Prix : ${pizza.price} €',
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}